#include "kaiba_robot/kaiba_drive.h"

KaibaDrive::KaibaDrive()
  : nh_priv_("~")
{
  //Init gazebo ros turtlebot3 node
  ROS_INFO("kaiba Simulation Node Init");
  auto ret = init();
  ROS_ASSERT(ret);
}

KaibaDrive::~KaibaDrive()
{
  updatecommandVelocity(0.0, 0.0);
  ros::shutdown();
}

/*******************************************************************************
* Init function
*******************************************************************************/
bool KaibaDrive::init()
{
  // initialize ROS parameter
  std::string cmd_vel_topic_name = nh_.param<std::string>("cmd_vel_topic_name", "");

  // initialize variables
  escape_range_       = 30.0 * DEG2RAD;
  check_forward_dist_ = 0.7;
  check_side_dist_    = 0.6;

  kaiba_pose_ = 0.0;
  prev_kaiba_pose_ = 0.0;

  // initialize publishers
  cmd_vel_pub_   = nh_.advertise<geometry_msgs::Twist>(cmd_vel_topic_name, 10);

  // initialize subscribers
  laser_scan_sub_  = nh_.subscribe("scan", 10, &KaibaDrive::laserScanMsgCallBack, this);
  odom_sub_ = nh_.subscribe("odom", 10, &KaibaDrive::odomMsgCallBack, this);

  return true;
}

void KaibaDrive::odomMsgCallBack(const nav_msgs::Odometry::ConstPtr &msg)
{
  double siny = 2.0 * (msg->pose.pose.orientation.w * msg->pose.pose.orientation.z + msg->pose.pose.orientation.x * msg->pose.pose.orientation.y);
	double cosy = 1.0 - 2.0 * (msg->pose.pose.orientation.y * msg->pose.pose.orientation.y + msg->pose.pose.orientation.z * msg->pose.pose.orientation.z);  

	kaiba_pose_ = atan2(siny, cosy);
}

void KaibaDrive::laserScanMsgCallBack(const sensor_msgs::LaserScan::ConstPtr &msg)
{
  uint16_t scan_angle[3] = {0, 30, 330};

  for (int num = 0; num < 3; num++)
  {
    if (std::isinf(msg->ranges.at(scan_angle[num])))
    {
      scan_data_[num] = msg->range_max;
    }
    else
    {
      scan_data_[num] = msg->ranges.at(scan_angle[num]);
    }
  }
}

void KaibaDrive::updatecommandVelocity(double linear, double angular)
{
  geometry_msgs::Twist cmd_vel;

  cmd_vel.linear.x  = linear;
  cmd_vel.angular.z = angular;

  cmd_vel_pub_.publish(cmd_vel);
}

/*******************************************************************************
* Control Loop function
*******************************************************************************/
bool KaibaDrive::controlLoop()
{
  static uint8_t kaiba_state_num = 0;

  switch(kaiba_state_num)
  {
    case GET_KAIBA_DIRECTION:
      if (scan_data_[CENTER] > check_forward_dist_)
      {
        if (scan_data_[LEFT] < check_side_dist_)
        {
          prev_kaiba_pose_ = kaiba_pose_;
          kaiba_state_num = KAIBA_RIGHT_TURN;
        }
        else if (scan_data_[RIGHT] < check_side_dist_)
        {
          prev_kaiba_pose_ = kaiba_pose_;
          kaiba_state_num = KAIBA_LEFT_TURN;
        }
        else
        {
          kaiba_state_num = KAIBA_DRIVE_FORWARD;
        }
      }

      if (scan_data_[CENTER] < check_forward_dist_)
      {
        prev_kaiba_pose_ = kaiba_pose_;
        kaiba_state_num = KAIBA_RIGHT_TURN;
      }
      break;

    case KAIBA_DRIVE_FORWARD:
      updatecommandVelocity(LINEAR_VELOCITY, 0.0);
      kaiba_state_num = GET_KAIBA_DIRECTION;
      break;

    case KAIBA_RIGHT_TURN:
      if (fabs(prev_kaiba_pose_ - kaiba_pose_) >= escape_range_)
        kaiba_state_num = GET_KAIBA_DIRECTION;
      else
        updatecommandVelocity(0.0, -1 * ANGULAR_VELOCITY);
      break;

    case KAIBA_LEFT_TURN:
      if (fabs(prev_kaiba_pose_ - kaiba_pose_) >= escape_range_)
        kaiba_state_num = GET_KAIBA_DIRECTION;
      else
        updatecommandVelocity(0.0, ANGULAR_VELOCITY);
      break;

    default:
      kaiba_state_num = GET_KAIBA_DIRECTION;
      break;
  }

  return true;
}

/*******************************************************************************
* Main function
*******************************************************************************/
int main(int argc, char* argv[])
{
  ros::init(argc, argv, "kaiba_drive");
  KaibaDrive kaiba_drive;

  ros::Rate loop_rate(125);

  while (ros::ok())
  {
    kaiba_drive.controlLoop();
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
